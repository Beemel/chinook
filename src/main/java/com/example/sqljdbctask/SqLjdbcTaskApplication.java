package com.example.sqljdbctask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SqLjdbcTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(SqLjdbcTaskApplication.class, args);
    }

}
