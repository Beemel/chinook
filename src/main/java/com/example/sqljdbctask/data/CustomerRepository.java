package com.example.sqljdbctask.data;

import com.example.sqljdbctask.models.Country;
import com.example.sqljdbctask.models.Customer;
import com.example.sqljdbctask.models.Invoices;
import com.example.sqljdbctask.models.PopularGenre;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@Service
public class CustomerRepository {
    @Value("${spring.datasource.url}")
    private String url;

    public List<Customer> getAllCustomers(){
        List<Customer> customers = new ArrayList<>();
        try(Connection con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement("SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer")){
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()){
                    customers.add(new Customer(rs.getInt("customer_id"), rs.getString("first_name"), rs.getString("last_name"), rs.getString("country"), rs.getString("postal_code"), rs.getString("phone"), rs.getString("email")));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public Customer getCustomerById(int id){
        Customer customer = null;
         try(Connection con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement("SELECT customer.customer_id, first_name, last_name, country, postal_code, phone, email FROM customer WHERE Customer_id= ?")){
            ps.setInt (1, id);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()){
                    customer = (new Customer(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }
    public List<Customer> getCustomerByName(String name){
        List<Customer> customer =new ArrayList<>();
        try(Connection con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement("SELECT customer.customer_id, first_name, last_name, country, postal_code, phone, email FROM customer WHERE first_name LIKE ?")){
            ps.setString (1, name);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()){
                    customer.add (new Customer(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    public List<Customer> getCustomerFirst(int amount){
        List<Customer> customer =new ArrayList<>();
        try(Connection con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement("   SELECT customer.customer_id, first_name, last_name, country, postal_code, phone, email FROM customer ORDER BY customer_id ASC LIMIT ?")){
            ps.setInt (1, amount);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()){
                    customer.add (new Customer(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }


    public Boolean addCustomer(Customer customer){
        Boolean add=false;
        try(Connection con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement("INSERT INTO customer (customer_id, first_name, last_name, country, postal_code, phone, email) VALUES (?, ?, ?, ?, ?, ?, ?)")){
            ps.setInt (1, customer.getCustomerId());
            ps.setString (2, customer.getFirstName());
            ps.setString (3, customer.getLastName());
            ps.setString (4, customer.getCountry());
            ps.setString (5, customer.getPostalCode());
            ps.setString (6, customer.getPhoneNumber());
            ps.setString (7, customer.getEmail());

            int result = ps.executeUpdate();
            add = (result != 0);

            }

        catch (SQLException e) {
            e.printStackTrace();
        }
        return add;
    }

    public Boolean updateCustomer(Customer customer){
        Boolean update=false;
        try(Connection con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement("UPDATE customer SET customer_id = ?, first_name = ?, last_name =?, country=?, postal_code =?, phone =?, email = ? WHERE customer_id = ?")){
            ps.setInt (1, customer.getCustomerId());
            ps.setString (2, customer.getFirstName());
            ps.setString (3, customer.getLastName());
            ps.setString (4, customer.getCountry());
            ps.setString (5, customer.getPostalCode());
            ps.setString (6, customer.getPhoneNumber());
            ps.setString (7, customer.getEmail());
            ps.setInt (8, customer.getCustomerId());

            int result = ps.executeUpdate();
            update = (result != 0);
        }

        catch (SQLException e) {
            e.printStackTrace();
        }
        return update;
    }
    public List<Country> countCountries(){
        List<Country> countries = new ArrayList<>();

        try(Connection con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement(" SELECT country, count(*) FROM customer GROUP by country ORDER BY count DESC")){
        try (ResultSet rs = ps.executeQuery()) {
            while (rs.next()){
                countries.add (new Country(rs.getString("country"), rs.getInt("count")));
              }
           }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return countries;
    }
    public List<Invoices> invoices(){
        List<Invoices> invoices = new ArrayList<>();

        try(Connection con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement(" SELECT customer.first_name, customer.last_name, SUM(invoice.total) FROM customer INNER JOIN invoice ON invoice.customer_id = customer.customer_id GROUP BY customer.customer_id ORDER BY sum DESC")){
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()){
                    invoices.add (new Invoices(rs.getString("first_name"), rs.getString("last_name"), rs.getInt("sum")));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return invoices;
    }
    public List<PopularGenre> popularGenres(int id){
        PopularGenre popularGenre = null;
        List<PopularGenre> popularGenres = new ArrayList<>();

        try(Connection con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement("    SELECT g.\"name\" as genre_name, COUNT (il.invoice_line_id) as genre_total FROM genre as g INNER JOIN track as t ON t.genre_id = g.genre_id INNER JOIN invoice_line as il ON il.track_id = t.track_id INNER JOIN invoice as inv ON inv.invoice_id = il.invoice_id INNER JOIN customer as c ON c.customer_id = inv.customer_id AND c.customer_id = ? GROUP BY g.genre_id, g.\"name\"ORDER BY genre_total DESC FETCH FIRST 1 ROWS with TIES")){
            ps.setInt (1, id);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()){
                    popularGenres.add (new PopularGenre(rs.getString("genre_name"), rs.getInt("genre_total")));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return popularGenres;
    }

}
