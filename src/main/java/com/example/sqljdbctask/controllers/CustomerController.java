package com.example.sqljdbctask.controllers;

import com.example.sqljdbctask.data.CustomerRepository;
import com.example.sqljdbctask.models.Country;
import com.example.sqljdbctask.models.Customer;
import com.example.sqljdbctask.models.Invoices;
import com.example.sqljdbctask.models.PopularGenre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping("/customers")
    public List<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @GetMapping("/customerid/{id}")
    public Customer getCustomerById(@PathVariable int id) {
        return customerRepository.getCustomerById(id);
    }

    @GetMapping("/customername/{name}")
    public List<Customer> getCustomerByName(@PathVariable String name) {
        return customerRepository.getCustomerByName(name);
    }

    @GetMapping("/customeramount/{amount}")
    public List<Customer> getCustomerByName(@PathVariable int amount) {
        return customerRepository.getCustomerFirst(amount);
    }

    @PostMapping("")
    public Boolean addCustomer(@RequestBody Customer customer) {
        return customerRepository.addCustomer(customer);
    }

    @PatchMapping("/customers/{id}")
    public Boolean updateCustomer(@PathVariable int id, @RequestBody Customer customer) {
        return customerRepository.updateCustomer(customer);
    }

    @GetMapping("/countcountries")
    public List<Country> countCountries() {
        return customerRepository.countCountries();
    }

    @GetMapping("/invoices")
    public List<Invoices> invoices() {
        return customerRepository.invoices();
    }

    @GetMapping("/populargenre/{id}")
    public List<PopularGenre> popularGenres(@PathVariable int id) {
        return customerRepository.popularGenres(id);
    }
}
