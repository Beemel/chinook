package com.example.sqljdbctask.models;

public class Invoices {
    private String firstName;
    private String lastName;
    private double invoices;

    public Invoices(String firstName, String lastName, double invoices) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.invoices = invoices;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getInvoices() {
        return invoices;
    }

    public void setInvoices(double invoices) {
        this.invoices = invoices;
    }
}
