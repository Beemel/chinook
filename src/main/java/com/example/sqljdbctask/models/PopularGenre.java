package com.example.sqljdbctask.models;

public class PopularGenre {
    private String genre;
    private int genre_total;


    public PopularGenre(String genre, int genre_total) {
        this.genre = genre;
        this.genre_total = genre_total;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getGenre_total() {
        return genre_total;
    }

    public void setGenre_total(int genre_total) {
        this.genre_total = genre_total;
    }

}
